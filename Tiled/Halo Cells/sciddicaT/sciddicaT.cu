/**
CUDA parallelization of the SciddicaT non-inertial fluid-flows simulation model
Tiled Implementation without Halo Cells

@author Mahbub Hasan [232759] 
@author Gebreyowhans Hailekiros Bahre [233619]

February 21,2022
*/


#include <cuda.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "util.hpp"

// ----------------------------------------------------------------------------
// I/O parameters used to index argv[]
// ----------------------------------------------------------------------------
#define HEADER_PATH_ID 1
#define DEM_PATH_ID 2
#define SOURCE_PATH_ID 3
#define OUTPUT_PATH_ID 4
#define STEPS_ID 5
// ----------------------------------------------------------------------------
// Simulation parameters
// ----------------------------------------------------------------------------
#define P_R 0.5
#define P_EPSILON 0.001
#define ADJACENT_CELLS 4
#define STRLEN 256
#define MASK_SIZE 3
#define TILED_SIZE 4
#define TILED_BLOCK_SIZE (TILED_SIZE+MASK_SIZE-1)

// ----------------------------------------------------------------------------
// Read/Write access macros linearizing single/multy layer buffer 2D indices
// ----------------------------------------------------------------------------
#define SET(M, columns, i, j, value) ((M)[(((i) * (columns)) + (j))] = (value))
#define GET(M, columns, i, j) (M[(((i) * (columns)) + (j))])
#define BUF_SET(M, rows, columns, n, i, j, value) ( (M)[( ((n)*(rows)*(columns)) + ((i)*(columns)) + (j) )] = (value) )
#define BUF_GET(M, rows, columns, n, i, j) ( M[( ((n)*(rows)*(columns)) + ((i)*(columns)) + (j) )] )

// ----------------------------------------------------------------------------
// I/O functions
// ----------------------------------------------------------------------------
void readHeaderInfo(char* path, int &nrows, int &ncols, /*double &xllcorner, double &yllcorner, double &cellsize,*/ double &nodata)
{
  FILE* f;
  
  if ( (f = fopen(path,"r") ) == 0){
    printf("%s configuration header file not found\n", path);
    exit(0);
  }

  //Reading the header
  char str[STRLEN];
  fscanf(f,"%s",&str); fscanf(f,"%s",&str); ncols = atoi(str);      //ncols
  fscanf(f,"%s",&str); fscanf(f,"%s",&str); nrows = atoi(str);      //nrows
  fscanf(f,"%s",&str); fscanf(f,"%s",&str); //xllcorner = atof(str);  //xllcorner
  fscanf(f,"%s",&str); fscanf(f,"%s",&str); //yllcorner = atof(str);  //yllcorner
  fscanf(f,"%s",&str); fscanf(f,"%s",&str); //cellsize = atof(str);   //cellsize
  fscanf(f,"%s",&str); fscanf(f,"%s",&str); nodata = atof(str);     //NODATA_value 
}

bool loadGrid2D(double *M, int rows, int columns, char *path)
{
  FILE *f = fopen(path, "r");

  if (!f) {
    printf("%s grid file not found\n", path);
    exit(0);
  }

  char str[STRLEN];
  for (int i = 0; i < rows; i++)
    for (int j = 0; j < columns; j++)
    {
      fscanf(f, "%s", str);
      SET(M, columns, i, j, atof(str));
    }

  fclose(f);

  return true;
}

bool saveGrid2Dr(double *M, int rows, int columns, char *path)
{
  FILE *f;
  f = fopen(path, "w");

  if (!f)
    return false;

  char str[STRLEN];
  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < columns; j++)
    {
      sprintf(str, "%f ", GET(M, columns, i, j));
      fprintf(f, "%s ", str);
    }
    fprintf(f, "\n");
  }

  fclose(f);

  return true;
}

double* addLayer2D(int rows, int columns)
{
  double *tmp = (double *)malloc(sizeof(double) * rows * columns);
  if (!tmp)
    return NULL;
  return tmp;
}

void copyDataFromHostToDevice(int row, int col, double *host_data, double *device_data){
    cudaError_t err = cudaMemcpy(device_data, host_data, row*col* sizeof(double ), cudaMemcpyHostToDevice);
    if (err != cudaSuccess) {
        printf("%s in %s at line %d\n", cudaGetErrorString(err),__FILE__,__LINE__);
        exit(EXIT_FAILURE);
    }
}

void copyDataFromDeviceToHost(double *device_data, double *host_data, int row, int col){
    cudaError_t err = cudaMemcpy(host_data, device_data, (row*col* sizeof(double )), cudaMemcpyDeviceToHost);
    if (err != cudaSuccess) {
        printf("%s in %s at line %d\n", cudaGetErrorString(err),__FILE__,__LINE__);
        exit(EXIT_FAILURE);
    }
}

// ----------------------------------------------------------------------------
// init kernel, called once before the simulation loop
// ----------------------------------------------------------------------------
__global__ void sciddicaTSimulationInit(int r, int c, double* Sz, double* Sh)
{
    int j = blockIdx.x * blockDim.x + threadIdx.x; // col
    int i = blockIdx.y * blockDim.y + threadIdx.y; // row
    if (i>0 && i<r-1 && j>0 && j<c-1){
        double z, h;
        h = GET(Sh, c, i, j);
        if (h > 0.0){
            z = GET(Sz, c, i, j);
            SET(Sz, c, i, j, z - h);
        }
    }
}

// ----------------------------------------------------------------------------
// computing kernels, aka elementary processes in the XCA terminology
// ----------------------------------------------------------------------------
__global__ void sciddicaTResetFlows(int r, int c, double nodata, double* Sf)
{
    int j = blockIdx.x * blockDim.x + threadIdx.x; // col
    int i = blockIdx.y * blockDim.y + threadIdx.y; // row
    if (i>0 && i<r-1 && j>0 && j<c-1){
        for (int index=0; index<4; index++){
            BUF_SET(Sf, r, c, index, i, j, 0.0);
        }
    }
}

__global__ void sciddicaTFlowsComputation(int r, int c, double nodata, double *Sz, double *Sh, double *Sf, double p_r, double p_epsilon)
{
    int tx = threadIdx.x;
    int ty = threadIdx.y;
    int bx = blockIdx.x;
    int by = blockIdx.y;
	
	//Each thread of the kernel first calculates the y and x indices of its output elements of the kernel.
    int row = by*TILED_SIZE + ty; // row
    int col = bx*TILED_SIZE + tx; // col
	
    // //Each thread then calculates the y and x indices of the input element it to load into the shared memory 
    int i = row-(MASK_SIZE/2);
    int j = col-(MASK_SIZE/2);

    // ----------------------------------------------------------------------------
    // DEFINE SHARED MEMORY
    // ----------------------------------------------------------------------------
    __shared__ double Sz_shared[TILED_BLOCK_SIZE][TILED_BLOCK_SIZE];
    __shared__ double Sh_shared[TILED_BLOCK_SIZE][TILED_BLOCK_SIZE];

    //// All Threads that participate in loading the input tiles in to the shared memory. Each thread checks if the y and x indecies of 
    // its tile elements are with in valid range of the input, otherwise they are treated as hallo cells with values 0.0f
    if ((i>=0 && i<r) && (j>=0 && j<c)){
        Sz_shared[ty][tx] = GET(Sz, c, i, j);
        Sh_shared[ty][tx] = GET(Sh, c, i, j);
    }else{
        Sz_shared[ty][tx] = 0.0f;
        Sh_shared[ty][tx] = 0.0f;
    }
    __syncthreads();

    int Xi[] = {0, -1,  0,  0,  1};// Xj: von Neuman neighborhood row coordinates (see below)
    int Xj[] = {0,  0, -1,  1,  0};// Xj: von Neuman neighborhood col coordinates (see below)
    bool eliminated_cells[5] = {false, false, false, false, false};
    bool again;
    double cells_count;
    double average;
    double m;
    double u[5];
    int n;
    double z, h;

    if (tx > 0 && tx < TILED_BLOCK_SIZE - 1 && ty > 0 && ty < TILED_BLOCK_SIZE - 1) {
        m = Sh_shared[ty][tx] - p_epsilon;
        u[0] = Sz_shared[ty][tx] + p_epsilon;
        for (int index = 1; index <= 4; index++) {
            z = Sz_shared[ty + Xi[index]][tx + Xj[index]];
            h = Sh_shared[ty + Xi[index]][tx + Xj[index]];
            u[index] = z + h;
        }
        do {
            again = false;
            average = m;
            cells_count = 0;

            for (n = 0; n < 5; n++) {
                if (!eliminated_cells[n]) {
                    average += u[n];
                    cells_count++;
                }
            }

            if (cells_count != 0) {
                average /= cells_count;
            }

            for (n = 0; n < 5; n++) {
                if ((average <= u[n]) && (!eliminated_cells[n])) {
                    eliminated_cells[n] = true;
                    again = true;
                }
            }
        } while (again);
        if (i>=0 && i<r && j>=0 && j<c){
            for (int index = 1; index <= 4; index++) {
                if (!eliminated_cells[index]) {
                    BUF_SET(Sf, r, c, (index - 1), i, j, (average - u[index]) * p_r);
                }
            }
        }
    }
}

__global__ void sciddicaTWidthUpdate(int r, int c, double nodata, double *Sz, double *Sh, double *Sf)
{

   int j = blockIdx.x * blockDim.x + threadIdx.x; // col
    int i = blockIdx.y * blockDim.y + threadIdx.y; // row
    if (i>0 && i<r-1 && j>0 && j<c-1){
        int Xi[] = {0, -1,  0,  0,  1};// Xj: von Neuman neighborhood row coordinates (see below)
        int Xj[] = {0,  0, -1,  1,  0};// Xj: von Neuman neighborhood col coordinates (see below)
        double h_next;
        h_next = GET(Sh, c, i, j);
        for(int index=1; index<=4;index++){
            h_next += BUF_GET(Sf, r, c, (4-index), i+Xi[index], j+Xj[index]) - BUF_GET(Sf, r, c, (index-1), i, j);
        }

        SET(Sh, c, i, j, h_next);
    }
}

// ----------------------------------------------------------------------------
// Function main()
// ----------------------------------------------------------------------------
int main(int argc, char **argv)
{
  int rows, cols;
  double nodata;
  readHeaderInfo(argv[HEADER_PATH_ID], rows, cols, nodata);

  int r = rows;                  // r: grid rows
  int c = cols;                  // c: grid columns
  //int i_start = 1, i_end = r-1;  // [i_start,i_end[: kernels application range along the rows
  //int j_start = 1, j_end = c-1;  // [i_start,i_end[: kernels application range along the rows
  double *Sz;                    // Sz: substate (grid) containing the cells' altitude a.s.l.
  double *Sh;                    // Sh: substate (grid) containing the cells' flow thickness
  double *Sf;                    // Sf: 4 substates containing the flows towards the 4 neighs
  double p_r = P_R;              // p_r: minimization algorithm outflows dumping factor
  double p_epsilon = P_EPSILON;  // p_epsilon: frictional parameter threshold
  int steps = atoi(argv[STEPS_ID]); //steps: simulation steps


  // The adopted von Neuman neighborhood
  // Format: flow_index:cell_label:(row_index,col_index)
  //
  //   cell_label in [0,1,2,3,4]: label assigned to each cell in the neighborhood
  //   flow_index in   [0,1,2,3]: outgoing flow indices in Sf from cell 0 to the others
  //       (row_index,col_index): 2D relative indices of the cells
  //
  //               |0:1:(-1, 0)|
  //   |1:2:( 0,-1)| :0:( 0, 0)|2:3:( 0, 1)|
  //               |3:4:( 1, 0)|
  //
  //

  /*
   * CUDA CALCULATION
   * 1. ASSIGN MEMORY FOR EACH CUDA VARIABLES
   * 2. COPY DATA FROM HOST TO DEVICE
   * 3. ASSIGN BLOCKS AND THREADS SIZE FOR EACH KERNELS EXECUTIONS
   */
    cudaMallocManaged(&Sz, sizeof(double) * r * c);
    cudaMallocManaged(&Sh, sizeof(double) * r * c);
    cudaMallocManaged(&Sf, sizeof(double) * ADJACENT_CELLS * r * c);

    loadGrid2D(Sz, r, c, argv[DEM_PATH_ID]);   // Load Sz from file
    loadGrid2D(Sh, r, c, argv[SOURCE_PATH_ID]);// Load Sh from file

    int block_x=TILED_BLOCK_SIZE;
    int block_y=TILED_BLOCK_SIZE;
    dim3 BLOCK(block_x,block_y);
    int grid_x = ceil((r + TILED_SIZE-1)/(float )TILED_SIZE);
    int grid_y = ceil((c + TILED_SIZE -1)/(float )TILED_SIZE);
    dim3 GRID(grid_x,grid_y);

    printf("Block size(%d,%d), and Grid size(%d,%d)\n",BLOCK.x,BLOCK.y,GRID.x,GRID.y);

  // Apply the init kernel (elementary process) to the whole domain grid (cellular space)
    sciddicaTSimulationInit<<<GRID,BLOCK>>>(r,c,Sz,Sh);
    cudaDeviceSynchronize();
  util::Timer cl_timer;
  // simulation loop
  for (int s = 0; s < steps; ++s){

      // Apply the resetFlow kernel to the whole domain

      sciddicaTResetFlows<<<GRID,BLOCK>>>(r,c,nodata,Sf); // sigma 0
      cudaDeviceSynchronize();
      // Apply the FlowComputation kernel to the whole domain

      sciddicaTFlowsComputation<<<GRID,BLOCK>>>(r, c, nodata, Sz, Sh, Sf, p_r, p_epsilon); // sigma 1
      cudaDeviceSynchronize();
      // Apply the WidthUpdate mass balance kernel to the whole domain

      sciddicaTWidthUpdate<<<GRID,BLOCK>>>(r,c,nodata,Sz,Sh,Sf); // sigma 2
      cudaDeviceSynchronize();
  }
  double cl_time = static_cast<double>(cl_timer.getTimeMilliseconds()) / 1000.0;
  printf("Elapsed time: %lf [s]\n", cl_time);

  // memory copy from device to host
    //copyDataFromDeviceToHost(d_Sh, Sh, r, c);
  // Save information to the file
  saveGrid2Dr(Sh, r, c, argv[OUTPUT_PATH_ID]);// Save Sh to file

  printf("Releasing memory...\n");
  cudaFree(Sz);
  cudaFree(Sh);
  cudaFree(Sf);

  return 0;
}
